# docker build -t registry.gitlab.com/laurent-croq/gitlab-ci-tests --push .

FROM alpine:3.18

RUN apk add ansible && addgroup runner && adduser -h /app -G runner -g runner -D -s /bin/sh runner

USER runner
